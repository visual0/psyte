# 🪄 Psyte

![Rust](https://camo.githubusercontent.com/0b67f2eb691b83144519058d27f3ae6104f24a760db25d4a0566c7c40f53731f/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f727573742d2532333030303030302e7376673f7374796c653d666f722d7468652d6261646765266c6f676f3d72757374266c6f676f436f6c6f723d7768697465)
![Database](https://img.shields.io/badge/-database-red?style=for-the-badge)

> Store bytes, not data!

Psyte (**Ps**eudo B**yte**) is an extremely inefficient, key-value, untyped, pseudo-database. Being data stored by bytes, it's completely up to the user using the database to decide how information are stored.

## 🔮 Why `pseudobyte`?

I **highly** discourage everyone to use my learning projects as something stable and usable as they are not meant for this purpuse. The aim of this project is to learn more about files and sockets in Rust.

You should not use `psyte` if:

- You need a stable and secure database,
- You are not comfortable with working with not working binaries,
- You must rely on this software for a long period of time.

Instead, you should contribute to `psyte` if:

- You like bad engineered code,
- The idea of a *byte-based* database attracts you.

## 🧭 Goals of this project

To keep me motivated, I wrote this section that containes milestones that I want to achive working on this project.

- **REPL** (Read, Execute, Print Line) to interact with the Psyte server,
- A set of **CRUD** (Create, Remove, Update, Delete) operations, +1 for logic `AND`s record filtering,
- A small, **lightweight** application protocol based on **TCP/IP** that put some rules when it comes to how data is sent to a client.
- An **easy-to-customize** database, with files one can edit to adjust the way data is stored.

To learn more about this project, please refer to the wiki.
