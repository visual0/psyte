use libs::data;

fn main() {
    let tbl: data::Table = data::Table::empty();
    let seriald = tbl.serial().unwrap();

    println!("{:?}", seriald);

    let tbl = data::Table::deserial(&seriald).unwrap();

    println!("{:?}", tbl);
}
