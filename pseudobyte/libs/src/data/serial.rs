use crate::data;

use super::Table;

use std::error::Error;

type Serial = Vec<u8>;

impl Table {
    pub fn serial(&self) -> Result<Serial, impl Error> {
        bincode::serialize(&self)
    }

    pub fn deserial(serial: &Serial) -> Result<Table, impl Error> {
        bincode::deserialize(serial)
    }
}