use super::*;

impl Table {
    pub fn new(table: Vec<Record>) -> Table {
        Table::Records { table }
    }

    pub fn empty() -> Table {
        Table::Empty
    }


} 