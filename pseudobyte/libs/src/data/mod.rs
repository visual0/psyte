use serde::{Serialize, Deserialize};

pub type Data = Vec<u8>;

#[derive(Serialize, Deserialize, Debug)]
pub enum Record {
    Standard {
        name: Data,
        value: Data,
    }
} 

#[derive(Serialize, Deserialize, Debug)]
pub enum Table {
    Records {
        table: Vec<Record>
    },
    Empty
}

pub mod serial;
pub mod impls;



